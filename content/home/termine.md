---
title: "Termine"
date: 2019-10-13T13:43:53+02:00
weight: 2
draft: false
---
### Aktuelle Termine

- [Krise der Nationalstaaten – anarchistische Antworten?](https://www.ph-freiburg.de/soziologie/veranstaltungen.html) Online-Tagung organisiert von der Pädagogischen Hochschule Freibug, 19.-21.03.2021
- [„Der Staat ist doof und stinkt“? Perspektiven linker und anarchistischer Staatskritik](https://freieassoziation.noblogs.org/staatskritik/) Tagung zu anarchistischer Staatskritik 9.-11. April 2021, online
- [Freie Assoziation? – Tagung zu antiautoritärem Kommunismus](https://freieassoziation.noblogs.org/kommunismus/) 28.-30. Mai, Göttingen (ggf. digital)

### Vergangenheit

- [Utopie-Konferenz](http://konferenzimforum.at/utopie/konferenz/) 09.-11.10.2020 Graz
- [Zukunft für Alle](https://zukunftfueralle.jetzt/) - Kongress für Utopien und Transformation 25.-30.08.2020 Leipzig und online 
- Autorenlesung "Warum haben wir eigentlich immer noch Kapitalismus?" mit P.M. 16.-20.03.2020 Halle, Magdeburg, Kassel, Berlin, Hamburg
- [10. Anarchietage in Winterthur](https://anarchietage.ch/2020) 7.-9.02.2020
