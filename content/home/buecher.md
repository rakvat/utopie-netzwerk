---
title: "Bücher"
date: 2019-10-13T13:43:53+02:00
weight: 8
draft: false
---

### Utopien allgemein

- Kapitalismus aufheben - Eine Einladung über Utopie und Transformation neu nachzudenken, Simon Sutterlütti und Stefan Meretz (2018): Kapitel 4 - Meta-Theorie zum Entwurf von Utopien; Kapitel 6 wird argumentiert wie eine Commons-Utopie jenseits von Markt und Staat (begriffen als eine Institution die Entscheidungen durchsetzen kann) aussehen könnte (frei verfügbar unter [commonism.us](https://commonism.us))
- Konkrete Utopien, Alexander Neupert-Doppler (Hrsg.) (2018): Ein Sammelband mit Utopien aus verschiedenen sozialen Bewegungen (Care Revolution, Commons, Autonomiekonzept, Recht auf Stadt, Postwachstum, etc.)
- Verein freier Menschen, Hannes Gießler (2018): Kritisiert, dass kommunistische Utopien durch ihre Marktgegenerschaft tendenziell einen autoritären Staatsapparat aufgebaut haben
- Utopie – Vom Roman zur Denkfigur, Alexander Neupert-Doppler (2015) – Einführung in die Geschichte und Funktionen des utopischen Denkens.
- Konstellationen negativ-utopischen Denkens, Jochen Gimmel (2015) – Zur Theorie des Utopischen.
- Etwas fehlt – Utopie, Kritik und Glücksversprechen, Jour fixe Initiative Berlin (2013) – Ein Sammelband zur Utopiedebatte.
- Utopia as method, Ruth Levitas (2013) – Grundlegendes zu Utopie als Schulung des Begehrens.
- [Die Revolution](https://ptgustavlandauer.files.wordpress.com/2016/09/landauer-die-revolution.pdf), Gustav Landauer (1907): Geschichtsbetrachtung, in der sich Topien und Utopien abwechseln

### Bewegungen

- Konkrete Utopie - Die Berge Kurdistans und die Revolution in Rojava, Lower Class Magazine (2017) – Zur kurdischen Bewegung.
- Widerstand und gelebte Utopien: Frauenguerilla, Frauenbefreiung und Demokratischer Konföderalismus in Kurdistan, Informationsstelle Kurdistan
e.V. (2017).
- Utopische Realpolitik – Die Neue Linke in Lateinamerika, Helge Buttkereit (2011) – Zum Sozialismus des 21. Jahrhunderts/Chiapas.
- Negativität und Utopie in der Bewegung für globale Gerechtigkeit, Michael Löwy (2010) in: Luxemburg – Zeitschrift Gesellschaftsanalyse
und linke Praxis.

### Bildung

- Das Utopische, David Salomon (2018) und Utopiekompetenz, Henrik Schröder (2018), in: Politische Ideen und politische Bildung, Ingo Juchler (Hg.).
- Erziehung, Utopie und der Wille zum Sozialismus, Simon Daemgen (in Neupert-Doppler 2018).
- Notizen zu Alltagsverstand, politischer Bildung und Utopie, Uwe Hirschfeld (2015)..
- Utopie denken – Realität verändern: Bildungsarbeit in den Gewerkschaften, Klaus Ahlheim & Horst Mathes (2011) – Sammelband.

### Feminismus

- Her mit der Zukunft?! Feministische und queere Utopien und die Suche nach alternativen Gesellschaftsformen, Femina Politica (2019).
- Von der Gesellschaftsanalyse zur Utopie - Ein historischer Rückblick auf materialistisch-feministische Theorien, Katharina Volk (2018).
- Care-Revolution - Schritte in einer solidarische Gesellschaft, Gabriele Winker (2015).

### Geschichte

- Die Rätebewegung in Österreich: Von sozialer Notwehr zur konkreten Utopie. Lederer, Anna/Pavlich, Andreas (2019).
- Utopie und Feminismus, Annemie Vanackere/Anne Reimann (2018) – Feminismus und Russische Revolution.
- Geschichte der Utopie, Thomas Schölderle (2012). 

### Migration

- Von No Lager bis No Border – (Konkrete) Utopien in der Refugee-Bewegung, Lisa Doppler (2018) (in Neupert-Doppler 2018).
- Solidarity City – Cizenship in der postmigrantischen Stadt, Matthias Rodatz (2018) (in Neupert-Doppler 2018)

### Ökologie

- Nachhaltigkeit als Utopie, Björn Wendt (2018).
- Kommende Nachhaltigkeit, Daniela Gottschlich (2018) - Nachhaltigkeit als Utopie (in: Neupert-Doppler 2018).
- Postwachstum als konkrete Utopie, Boris Heil (2018) (in Neupert-Doppler 2018).
- Zusammenspiel mit der Natur – Wirklichkeit und Utopie einer spielerischen Technik, Jan Friedrich (2015) – Zu Technik und Natur.
- Gut leben – eine Gesellschaft jenseits des Wachstums, Barbara Muraca (2014).

### Organisationsformen

- Genossenschaften und Utopie, Gisela Notz (in Neupert-Doppler 2018).
- Arbeit und Utopie, Joachim Beerhorst (in Neupert-Doppler 2018) – Zu Utopie und Gewerkschaften.
- Institutionen aufheben, Martin Birkner (in Neupert-Doppler 2018) – Institutionen im Postkapitalismus.
- [Utopia ist machbar](http://www.utopia-ist-machbar.de/utopia/downloads/UTOPIA%20ist%20machbar.pdf), Peter Lucas (2016): Ein leicht lesbarer eher auspinselnder Entwurf einer möglichen Utopie.
- [Beitragen statt Tauschen](https://peerconomy.org/text/peer-oekonomie.pdf), Christian Siefkes (2008): Peer Production: Alle arbeiten worauf sie Lust haben und teilen ihre Erträge mit anderen. Wie kann das nicht nur für immaterielle Güter funktionieren, sondern als Grundlage der materiellen Produktion einer Gesellschaft. Für die Tätigkeiten wo sich niemand findet, wird Belohnung eingeführt.
- [Towards an Inclusive Democracy](https://www.inclusivedemocracy.org/fotopoulos/english/brbooks/brtid/IDBook.pdf), Takis Fotopolous (1997): Räumlich dezentrale demokratische Planung der Grundbedürfnissbefriedigung. Darüberhinausgehende Bedürfnisse über einen "künstlichen" Markt vermittelt, um sowohl Bedürfnisbefriedigung als auch Wahlfreiheit zu gewährleisten. Wer sich nur für das Modell interessiert: 6. Kapitel, S. 255-274
- [Workers' Councils and the Economics of Self-Managed Society](https://libcom.org/library/workers-councils-economics-self-managed-society-cornelius-castoriadis), Cornelius Castoriadis (1957): Ausführliche Beschreibung eines Rätemodells."Workers Council and Economics..."
- [Grundprinzipien kommunistischer Produktion und Verteilung](http://www.mxks.de/files/kommunism/gik.html), Gruppe Internationaler Kommunisten Hollands (1930): Rätemodell als Gegenmodell zu zentraler Planung, um damit verbundene Herrschaft wie im Realsozialismus zu vermeiden
- [Panarchie - Eine verschollene Idee von 1860](https://www.panarchy.org/nettlau/1909.de.html), Max Nettlau (1909): Über die Möglichkeit des Nebeneinanders unterschiedlicher Gesellschaftsformen.
- [Anarchie](https://www.anarchismus.at/anarchistische-klassiker/errico-malatesta/6639-errico-malatesta-anarchie), Errico Malatesta (1889): Grundsätzliche Erklärungen über Anarchie, Argumente gegen Staat, Grundrisse einer anarchistischen Gesellschaft.

#### Ökonomie

- Die machbare Utopie, Albert Michael (2017) – Grundlagen einer Partizipatorischen Ökonomie.
- Reale Utopien - Wege aus dem Kapitalismus, Erik O. Wight (2017) – Gelebte Utopien alternativen Wirtschaftens.
- Postkapitalismus, Paul Mason (2016) – Skizzen einer postkapitalistischen Ökonomie.
- Theorien alternativen Wirtschaftens. Fenster in eine andere Welt, Gisela Notz (2012): Einführungsband exemplarischer Theorien alternativen Wirtschaftens und ihrer Umsetzung. Begriffserklärung, Geschichte, Gegenwart und wünschenswerte Zukunft.
- Parecon: Life After Capitalism, Michael Albert (2003, dt.: Trotzdem Verlag 2006): Dezentrale Planung, die aber nicht in räumlich dezentralen Einheiten stattfindet. Individuelle Pläne werden in einem Prozess zu einem gesellschaftlichen Gesamtplan zusammengefügt. Deutsche Übersetzung z.T. inhaltlich vom Original abweichend, deswegen besser die englische Originalversion lesen. Wer sich Langeweile sparen will, sollte bei Teil 2 des Buches anfangen.
  - [Auseinandersetzung zwischen Christian Siefkes und Michael Albert auf keimform.de](https://keimform.de/2013/parecon-versus-peer-produktion-teil-1)

##### Planwirtschaft

"Planwirtschaft" wird oft mit autoritärer zentraler Planung assoziiert, welche wir ablehnen. Jedoch kann die Methode auch in dezentralen Strukturen und mit basisdemokratischer Entscheidungsfindung genutzt werden.

- The People’s Republic of Walmart, Leigh Phillips and Michal Rozworski (2019): Viele transnationale Unternehmen agieren mit Methoden der Planwirtschaft und beweisen so, dass diese im großen Maßstab möglich ist
- [Bedürfnisorientierte Versorgungswirtschaft](https://gruppew8.files.wordpress.com/2009/02/bvw.pdf), Alfred Fresin (2005): Zentrale Planung der Produktion zur Sicherstellung der Bedürfnisbefriedigung.
- Towards a New Socialism (dt. Alternativen aus dem Rechner), Paul Cockshott/ Allin Cottrell (1993, dt. 2006): Modell demokratischer Planung der Ökonomie ohne Räte. Wie kann die Berechnung von Arbeitswerten der produzierten Güter stattfinden, ist eine zentrale Planung der Produktion mittels Computer beherrschbar.
- [In Defense of Socialist Planning](https://www.marxists.org/archive/mandel/1986/09/planning.html) (dt.: Zur Verteidigung der sozialistischen Planwirtschaft), Ernest Mandel, 1984: Zentrale Planung plus Demokratie in einer geldlosen Wirtschaft. Reaktion auf die Kritik von Marktsozialisten, dass eine Planung der Wirtschaft nicht möglich sei. Wer sich nur für das Modell interessiert: Kapitel 7, S. 26-32

### Stadt

- Von wegen – Überlegungen zur freien Stadt der Zukunft, Nils Boeing (2015) – Recht auf Stadt als konkrete Utopie.
- Recht auf die Stadt: Von Selbstverwaltung und radikaler Demokratie, Daniel Mullis (2014).
- Die Geschichte von der guten Stadt: Politische Philosophie zwischen urbaner Selbstverständigung und Utopie, Maria-Daria Cojocarus,
Maria-Daria (2012).

### Transformation & Revolution

- Beziehungsweise Revolution, Bini Adamczak (2018): Reflexionen auf 1918 und 1968 mit der Analyse, dass Konstruktionsprozesse und das Schaffen neuer Beziehungsformen entscheidend für revolutionäre Prozesse sind.
- Erneuter Versuch über die Befreiung., Jour fixe Initiative Berlin (2018) – Gespräch über die Zukunft nach dem Kapitalismus.
- [Ecommony](https://keimform.de/wp-content/uploads/2016/06/Habermann_Ecommony.pdf), Friederike Habermann (2016): UmCare zum Miteinander: Denkt die Gegenwart positiv weiter, hin zu einer commonistischen Gesellschaft
- Care Revolution, Gabriele Winker (2015): Gabriele Winker entwickelt Schritte in eine solidarische Gesellschaft, die nicht mehr Profitmaximierung, sondern menschliche Bedürfnisse und insbesondere die Sorge umeinander ins Zentrum stellt. Ziel ist eine Welt, in der sich Menschen nicht mehr als Konkurrent_innen gegenüberstehen, sondern ihr je individuelles Leben gemeinschaftlich gestalten.
