---
title: "Utopie Netzwerk"
date: 2019-10-13T13:43:27+02:00
weight: 1
draft: false
---

Wir, das Utopie Netzwerk, verstehen unter Utopien Gesellschaftsentwürfe, die die Mindestanforderungen Freiheit, Solidarität und Inklusion erfüllen und realisierbar sind.

Im Utopie Netzwerk organisieren sich emanzipatorische Linke/Linksradikale um über die befreite Gesellschaft zu diskutieren, nachzudenken und solidarisch zu streiten. Das Bilderverbot ist vorüber. Und es war nie als Denkverbot gemeint. Seinen kritischen Impuls gilt es jedoch aufzunehmen: Wir pinseln keine Utopien aus, sondern diskutieren die Grundlagen einer Gesellschaft jeneits von Markt, Arbeitszwang, Ausgrenzung, Patriarchat, Vereinzelung, Staat und Herrschaft.

Das Nachdenken über Alternativen zum kapitalistischen System scheint uns wegen des durch Kapitalismus verursachten Klimaumbruchs, wiederkehrender ökonomischer Krisen und globaler Verteilungsungerechtigkeiten als dringend erforderlich. Durch das Konkretisieren eines Netzwerks von realisierbaren Gesellschaftsentwürfen, wollen wir Ideen und Strategieren für politische Praxis und Wege zur Transformation entwickeln.

Das Netzwerk ist keiner bestimmten Tradition verpflichtet, bezieht seine historischen Einflüsse jedoch aus den dissidenten Teilen der anarchistischen, kommunistischen und sozialistischen Linken und der radikalen Bewegungen, die gegen Staatsgläubigkeit und Parteifetisch Autonomie und Arbeitskritik gesetzt haben.

