---
title: "Texte"
date: 2019-10-13T13:43:53+02:00
weight: 5
draft: false
---


- [„Rufen was nicht ist“ – Zur Utopie und ihrer Rolle für die Emanzipation](https://translibleipzig.wordpress.com/2019/12/03/rufen-was-nicht-ist-zur-utopie-und-ihrer-rolle-fur-die-emanzipation/), translib (2019): Über Notwendigkeit und Kriterien konkreter Utopien
- [Dezentrale Utopien und globale Herausforderungen](https://www.graswurzel.net/gwr/2019/12/dezentrale-utopien-und-globale-herausforderungen), Katharina Tidesh (2019): Aufruf zum Diskutieren unterschiedlicher Utopieansätze und Möglichkeiten des Nebeneinanders unterschiedlicher Konzepte
- [«Ich halte das anti-utopische Bilderverbot für erledigt»](http://www.schmetterling-verlag.de/page-5_isbn-3-89657-199-0.htm), Gespräch Adamczak/Neupert-Doppler (2018)
- [Umrisse der Welcommune](https://kosmoprolet.org/de/umrisse-der-weltcommune), Freundinnen und Freunde der klassenlosen Gesellschaft (2018): Der Text argumentiert weshalb Utopien für eine revolutionäre Überwindung wichtig und notwendig sind und macht einige feine Pinselstriche in Richtung einer Utopie mit Rät_innen.
  - Eine solidarische Kritik dazu: [Warum brauchen wir noch Räte wenn die eh nichts durchsetzen sollen?](https://keimform.de/2018/eine-verstaendigung-ueber-die-grundzuege-einer-klassenlosen-gesellschaft-ist-allemal-sinnvoll/), Simon Sutterlütti
  - Rüdiger Mats [kommentiert eine Diskussion zwischen den Freund_innen der klassenlosen Gesellschaft und Gießler in Leipzig und diskutiert Kernargumente](https://jungle.world/artikel/2019/08/tschuess-commune)
- Schönwetter-Utopien im Crashtest, Annette Schlemm (2017): Wie sollten Utopien angesichts der immer schneller voranschreitende ökologische Katastrophe aussehen?
- Behrend, Hanna (1997): Rückblick aus dem Jahr 2000 - Was haben Gesellschaftsutopien uns gebracht? In Hanna Behrend (Hrsg.): Auf der Suche nach der verlorenen Zukunft. Band 4. Berlin: trafo-verlag. S. 11-24: Über die Notwendigkeit der "Reaktivierung des gelähmten Utopiebewusstseins" nach dem Ende der realsozialistischen Staaten
- Isolde Neupert-Köpsel (1997): Die Bedeutung postmoderner Theorieaspekte in der feministischen/weiblichen Utopiedebatte. In Hanna Behrend (Hrsg.): Auf der Suche nach der verlorenen Zukunft. Band 4. Berlin: trafo-verlag. S. 129-146: Über die Neuartigkeit des Utopischen in insb. feministischen Utopien der letzten Jahrzehnte.

