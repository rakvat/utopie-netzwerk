---
title: "Musik"
date: 2019-10-13T13:43:35+02:00
weight: 4
draft: false
---

### Berlin 2070, Revolte Springen

{{< youtube NZqpOsMzh4A >}}

### Working on Wonderland, Sookee

{{< youtube mNh5eg3DSrM >}}

### Hurra, die Welt geht unter, KIZ

{{< youtube _Qq6xPbdoag >}}


### Imagine, John Lennon

{{< youtube mfHb-avHjO0 >}}


### Angst und Eigentum, Funny van Dannen

{{< youtube xvEHWsyXwcc >}}


### Fridays for Future, Merkels Sohn

{{< youtube L2RJjYy5-3M >}}


### Wir sind Kinder einer Erde, Grips Theater

{{< youtube xJFY-huoFW4 >}}
