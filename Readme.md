# DEPRECATED

https://utopie-netzwerk.de has now switched to a Wordpress page.

# Website des Utopie Netzwerks

http://utopie-netzwerk.de

Erstellt mit https://gohugo.io/



## Inhalte ändern

1. Unter https://gitlab.com/rakvat/utopie-netzwerk/tree/master/content/home eine Kategorie auswählen und die Datei anklicken.
1. Klicke "Edit"
1. Änderungen vornehmen
1. Klicke "Commit changes"
1. 1-2 Minuten warten
1. Prüfen, ob die neuen Inhalte korrekt unter https://utopie-netzwerk.de dargestellt werden
1. Wenn etwas kaputt ist, bitte Katja Bescheid sgen
