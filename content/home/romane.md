---
title: "Romane"
date: 2019-10-13T13:43:53+02:00
weight: 3
draft: false
---


- Planet der Habenichtse, Ursula K. Le Guin, Science Fiction, Realisierung einer anarchistischen Utopie ohne Arbeitszwang und deren Herausforderungen
- Bolo'bolo, P.M. (1983): Eher auspinselnder Entwurf einer föderalen anarchistischen Gesellschaft, die stark auf Subsistenz setzt.
- Walkaway, Cory Doctorov, Science Fiction, Post-scarcity/Post-Knappheits Utopie in kleineren bis mittleren Gruppen ohne Arbeitszwang und Eigentum
- Darknet&Freedom, Daniel Suarez: Ein von einem Spieleentwickler geschriebenes Computerprogramm versucht auf blutige Weise eine neue Weltordnung zu etablieren
- Voyage from Yesteryear, James P. Hogan, Science Fiction. Über das Entstehen einer anarchistischen Adhokratie in einer Post-Scarcity Gesellschaft ohne Konditionierung.
- Glimpses Into the Year 2100 (50 years after the revolution), Ilan Shalif 2007: Entwurf einer dezentralen Gesellschaft in Zeiten des Klimaumbruchs [The Anarchist Library](https://theanarchistlibrary.org/library/ilan-shalif-glimpses-into-the-year-2100-50-years-after-the-revoution)
