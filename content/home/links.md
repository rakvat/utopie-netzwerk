---
title: "Links"
date: 2019-10-13T13:43:48+02:00
weight: 6
draft: false
---

- [keimform.de](https://keimform.de) Blog über Commons, Keimformtheorie und Commonismus
- [konzeptwerk-neue-oekonomie.org](https://konzeptwerk-neue-oekonomie.org) Netzwerk zu den Themen Degrowth und alternative Ökonomien
- [transform-social.org](https://transform-social.org) Gedanken zu Utopien, Transformation und Umsetzungsversuche im jetzigen System
- [system-change.net](https://system-change.net/) Materialien und Workshops zum Systemwandel aus Perspektive der Klimagerechtigkeitsbewegung



